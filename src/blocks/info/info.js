$('.info').slick({
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        arrows: false,
        dots: true,
        adaptiveHeight: true
      }
    },
  ]
});
