jQuery(document).ready(function($) {

  $('[data-fancybox]').fancybox({
    afterShow: function() {
      if ($('.slider').length) {
        $('.slider').slick({
          dots: false,
          arrows: true,
          infinite: false,
          slidesToShow: 3,
          slidesToScroll: 1,
          responsive: [
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
              }
            },
            {
              breakpoint: 576,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
              }
            },
          ]
        })
      }
    },
    afterClose: function() {
      if ($('.slider').length) {
        $('.slider').slick('unslick')
      }
    }
  })
  
})
