$('.audio').each(function() {
  var media = $(this).find('.audio__hidden');
  
  var play = $(this).find('.audio__btn');
  var audio = $(this).find('.audio__hidden');
  var percentage = $(this).find('.audio__progress-played');
  var seekObj = $(this).find('.audio__progress');
  var currentAudioTime = $(this).find('.audio__progress-time');

  function togglePlay() {
    if (media[0].paused === false) {
      media[0].pause();
      play.removeClass('pause');
    } else {
      media[0].play();
      play.addClass('pause');
    }
  }

  function calculatePercentPlayed() {
    var progress = (media[0].currentTime / media[0].duration).toFixed(2) * 100;
    percentage.css('width', progress)
  }

  function calculateCurrentValue(currentTime) {
    const currentMinute = parseInt(currentTime / 60) % 60;
    const currentSecondsLong = currentTime % 60;
    const currentSeconds = currentSecondsLong.toFixed();
    const currentTimeFormatted = `${currentMinute < 10 ? `0${currentMinute}` : currentMinute}:${
    currentSeconds < 10 ? `0${currentSeconds}` : currentSeconds
    }`;

    return currentTimeFormatted;
  }

  function initProgressBar() {
    const currentTime = calculateCurrentValue(media[0].currentTime);
    currentAudioTime.text(currentTime);
    seekObj.bind('click', seek);

    media[0].onended = () => {
      play.removeClass('pause');
      percentage.style.width = 0;
      currentAudioTime.innerHTML = '00:00';
    };

    function seek(e) {
      const percent = e.offsetX / this.offsetWidth;
      media[0].currentTime = percent * media[0].duration;
    }

    calculatePercentPlayed();
  }

  play.bind('click', togglePlay);
  audio.bind('timeupdate', initProgressBar);
});
