jQuery(document).ready(function($) {

  var formSelect = $('.select__input')
  
  formSelect.select2({
    minimumResultsForSearch: -1,
    width: '100%',
    escapeMarkup: function(markup) {
      return markup
    }
  })
  
})
