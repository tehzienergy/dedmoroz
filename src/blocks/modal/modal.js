$('[data-fancybox]').fancybox({
  touch: false,
  autoFocus: false
})

$('.modal__close').click(function(e) {
  e.preventDefault();
  parent.jQuery.fancybox.getInstance().close();
})
