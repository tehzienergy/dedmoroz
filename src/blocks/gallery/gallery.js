$('.gallery').slick({
    centerMode: true,
    centerPadding: '12.3%',
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    infinite: true,
    arrows: false,
    variableWidth: false,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          centerPadding: '3%'
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          centerPadding: '3%'
        }
      },
      {
        breakpoint: 576,
        settings: {
          centerPadding: '20px',
          slidesToShow: 1
        }
      }
    ]
  });