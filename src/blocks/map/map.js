var locations = [
  {
    lat: 55.80043412,
    lng: 37.48201732
  },
  {
    lat: 55.67957415,
    lng: 37.70174388
  },
  {
    lat: 55.92300581,
    lng: 37.77462230
  },
  {
    lat: 68.99156512,
    lng: 33.04783724
  },
  {
    lat: 56.83920031,
    lng: 53.26569204
  },
  {
    lat: 56.80908048,
    lng: 53.20252066
  }
]


function initMap() {

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 5,
    center: {
      lat: 55.80043412,
      lng: 37.48201732
    }
  });

  // Add some markers to the map.
  // Note: The code uses the JavaScript Array.prototype.map() method to
  // create an array of markers based on a given "locations" array.
  // The map() method here has nothing to do with the Google Maps API.
  var markers = locations.map(function (location, i) {
    return new google.maps.Marker({
      position: location,
      icon: 'img/pin1.png'
    });
  });

  // Add a marker clusterer to manage the markers.
  var markerCluster = new MarkerClusterer(map, markers, {
    imagePath: 'img/pin'
  });


//  $('.fest-map__zoom-plus').click(function(e) {
//    e.preventDefault();
//    map.setZoom(map.getZoom() + 1);
//  });
//
//  $('.fest-map__zoom-minus').click(function(e) {
//    e.preventDefault();
//    map.setZoom(map.getZoom() - 1);
//  });
//  
//  infoWindow = new google.maps.InfoWindow;

  // Try HTML5 geolocation.
//  if (navigator.geolocation) {
//    navigator.geolocation.getCurrentPosition(function(position) {
//      var pos = {
//        lat: position.coords.latitude,
//        lng: position.coords.longitude
//      };
//
//      infoWindow.setPosition(pos);
//      infoWindow.open(map);
//      map.setCenter(pos);
//    });
//  }
}
